#!/bin/bash

export RONIN_LANG_SUB=lib
export RONIN_RECIPE=kyogen

export RONIN_OS_DIR=$NOH_ROOT/bunraku
export RONIN_CHARMS_DIR=$NOH_ROOT/kabuki
export RONIN_SDK_DIR=$NOH_ROOT/sdk

################################################################################

export COCKPIT_FUNC_SET="web x11"

export JINCHURIKI_SHIELD=yes
export JINCHURIKI_TAILS=""
export JINCHURIKI_PATHs="ipv6 alter vpn"

################################################################################

export RONIN_LANG_DIR=$RONIN_ROOT/lib
export RONIN_COOKBOOK=$NOH_ROOT/$RONIN_RECIPE
